import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { FormsModule } from "@angular/forms";

import { AppComponent } from "./app.component";
import { UsersComponent } from "./users/users.component";
import { HomeComponent } from "./home/home.component";
import { HttpClientModule } from "@angular/common/http";
import { PostsComponent } from "./posts/posts.component";
import { Hrefify } from "./pipes/href.pipe";
import { Capitalize } from "./pipes/capitalize.pipe";
import { UserComponent } from "./users/user.component";
import { PhotosComponent } from "./photos/photos.component";
import { NotFoundComponent } from "./notfound/notfound.component";

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    UserComponent,
    HomeComponent,
    PostsComponent,
    PhotosComponent,
    Hrefify,
    Capitalize,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      { path: "", component: HomeComponent, pathMatch: "full" },
      { path: "users", component: UsersComponent },
      { path: "user/:id", component: UserComponent },
      { path: "posts", component: PostsComponent },
      { path: "photos", component: PhotosComponent },
      { path: "404", component: NotFoundComponent },
      { path: "**", redirectTo: "404", pathMatch: "full" }
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
