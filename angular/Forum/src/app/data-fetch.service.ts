import { Injectable } from "@angular/core";
import { User, Post, Image } from "./models/data.model";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable()
export class FetchData {
  private _url: string = "http://jsonplaceholder.typicode.com/";

  constructor(private _http: HttpClient) {}

  getUsers(): Observable<User[]> {
    return this._http.get<User[]>(this._url + "users");
  }

  getUser(id: number): Observable<User> {
    return this._http.get<User>(this._url + "users/" + id);
  }

  getPosts(): Observable<Post[]> {
    return this._http.get<Post[]>(this._url + "posts");
  }

  getImages(): Observable<Image[]> {
    return this._http.get<Image[]>(this._url + "photos?_limit=9");
  }

  getImage(id: number): Observable<Image> {
    return this._http.get<Image>(this._url + "photos/" + id);
  }
}
