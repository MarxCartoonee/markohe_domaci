import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { PurchaseForm } from "./form.model";
import { COUNTRYLIST } from "./countries";
import { Country } from "./country.model";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent implements OnInit {
  title = "app";
  today = new Date();
  purchaseForm: FormGroup;
  submitted: boolean = false;
  purchaseData: PurchaseForm;
  countryList = COUNTRYLIST;

  ngOnInit(): void {
    this.purchaseForm = new FormGroup({
      'billingData': new FormGroup({
        'firstName': new FormControl(null, Validators.required),
        'lastName': new FormControl(null, Validators.required),
        'email': new FormControl(null, [Validators.required, Validators.email]),
        'address': new FormControl(null, Validators.required),
        'city': new FormControl(null, Validators.required),
        'state': new FormControl(null),
        'zipCode': new FormControl(null, Validators.required),
        'country': new FormControl(null, Validators.required)
      }),
      'cardData': new FormGroup({
        'creditCard': new FormControl(null, [Validators.required, Validators.minLength(12), Validators.maxLength(19), this.notANumber.bind(this)]),
        'expMonth': new FormControl(null, [Validators.required, Validators.min(1), Validators.max(12), this.notANumber.bind(this)]),
        'expYear': new FormControl(null, [Validators.required, Validators.min(this.today.getFullYear()), this.notANumber.bind(this)]),
        'securityCode': new FormControl(null, [Validators.required, Validators.minLength(3), Validators.maxLength(4), this.notANumber.bind(this)]),
        'cardType': new FormControl('visa', Validators.required)
      }),
      'referralData': new FormGroup({
        'referral': new FormControl('friend')
      })
    });
  }

  onSubmit() {
    // console.log(this.purchaseForm);
    if (this.purchaseForm.valid) {
      this.purchaseData = new PurchaseForm();
      this.purchaseData.firstName = this.purchaseForm.get('billingData.firstName').value;
      this.purchaseData.lastName = this.purchaseForm.get('billingData.lastName').value;
      this.purchaseData.email = this.purchaseForm.get('billingData.email').value;
      this.purchaseData.address = this.purchaseForm.get('billingData.address').value;
      this.purchaseData.city = this.purchaseForm.get('billingData.city').value;
      this.purchaseData.state = this.purchaseForm.get('billingData.state').value;
      this.purchaseData.zipCode = this.purchaseForm.get('billingData.zipCode').value;
      this.purchaseData.country = this.purchaseForm.get('billingData.country').value;

      this.purchaseData.creditCard = this.purchaseForm.get('cardData.creditCard').value;
      this.purchaseData.expMonth = this.purchaseForm.get('cardData.expMonth').value;
      this.purchaseData.expYear = this.purchaseForm.get('cardData.expYear').value;
      this.purchaseData.securityCode = this.purchaseForm.get('cardData.securityCode').value;
      this.purchaseData.cardType = this.purchaseForm.get('cardData.cardType').value;

      this.purchaseData.referral = this.purchaseForm.get('referralData.referral').value;

      // console.log(this.purchaseData);
      this.purchaseForm.reset();
      this.submitted = true;
    }
  }

  isValid(element: FormControl): boolean {
    return element.invalid && (element.touched || element.dirty) ? true : false;
  }

  notANumber(control: FormControl): { [s: string]: boolean } | null {
    return (control.value && isNaN(parseInt(control.value))) ? { 'not-a-number': true } : null;
  }
}
