console.log("Školski primeri:");
console.log("---");

console.log("Marko Herćan");

var myName = "Marko",
    mySurname = "Herćan",
    myColor = "Orange";
console.info(myName);
console.error(mySurname);
console.error(myColor);

console.log("---");
console.log("Primeri sa objektom i funkcijama:");
console.log("---");

var user = {
    name: "Marko",
    surname: "Herćan",
    color: "Orange"
}

function zadatak01() {
    var fullName = user.name + " " + user.surname;
    console.log(fullName);
}

function zadatak02() {
    console.info(user.name);
    console.warn(user.surname);
    console.error(user.color);
}

zadatak01();
zadatak02();

console.log("---");
console.log("Primeri sa klasom i metodama:");
console.log("---");

class User {
    constructor(firstName, lastName, favColor) {
        this.name = firstName;
        this.surname = lastName;
        this.color = favColor;
    }

    zadatak01() {
        this.myself = document.getElementById("zadatak10");
        this.fullName = this.name + " " + this.surname;
        this.myself.innerHTML = this.fullName;
        console.log(this.fullName);
    }

    zadatak02() {
        this.myself = document.getElementById("zadatak11");
        this.myself.innerHTML = markoH.name + "\n" + markoH.surname + "\n" + markoH.color;
        console.info(this.name);
        console.warn(this.surname);
        console.error(this.color);
    }
}

var markoH = new User("Marko", "Herćan", "Orange");

markoH.zadatak01();
markoH.zadatak02();