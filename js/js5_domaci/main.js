// TODO
// 1. Formirati niz sa imenima nekoliko polaznika prekvalifikacije.
// Uz pomoć komade document.write() odštampati html tabelu sa imenima iz formiranog niza.
// Imena bi trebalo da su poređana po alfabetu
//
// 2. Na prethodni zadatak dodati da ime svakog drugog polaznika bude boldovano
//
// 3. Na kraj niza dodati ime predavača.
// Uveriti se da ime predavača uvek bude na kraju, bez obzira na sortiranje.
//
// Kreirati matricu 5x5 koja sadrži random brojeve od 1 do 100
// koristeći funkciju Math.random
// 
// Odštampati zbir svih elemenata matrice
//
// Odštampati najveći element na dijagonali matrice
// 
// Odštampati najmanji element na dijagonali matrice

var students = [
        "Đorđeta Sfrančok",
        "Julijana Ugrinov",
        "Ervin Senji",
        "Filip Varadinac",
        "Bogdan Bajčetić",
        "Dragan Mandić",
        "Predrag Zarić",
        "Nebojša Ninković",
        "Goran Nikolić",
        "Nemanja Marković",
        "Mića Jolić",
        "Marko Herćan",
        "Čedomir Urukalo",
        "Maria Oklobdžija",
        "Mihajlo Radmilović",
        "Bojana Gligorović"
    ],
    teachers = [
        "Milan Milićev",
        "Dragana Ristić",
        "Nebojša Marinkov",
        "Dragan Krstevski"
    ],
    min = 1,
    max = 100,
    matrix = lotsOfGuns(5, 5, min, max),
    sum = enterThe(matrix)[0],
    diag = enterThe(matrix)[1],
    high = followTheWhiteRabbit(diag)[0],
    low = followTheWhiteRabbit(diag)[1];


document.write(tabled(students, randName(teachers)));

document.write(ignoranceIsBliss(matrix, sum, high, low));


function tabled(list, name) { // For any array
    var out = "<table><tr>",
        colspan = "";
    list.sort().push(name); // Sort the array and add second parameter to end of table
    for (var i = 0; i < list.length; i++) { // Write into HTML table
        if (i + 1 == list.length) colspan = " colspan=4"; // Make final element cell fill the row
        out += "<td" + colspan + ">";
        if ((i + 1) % 2 == 0) out += "<strong>" + list[i] + "</strong>"; // Make every even value bold
        else out += list[i];
        out += "</td>";
        if ((i + 1) % 4 == 0) out += "</tr><tr>";
    }
    out += "</tr><table>";
    return out;
}

function randName(list) { // For any array, return a random value
    var n = thereIsNoSpoon(0, list.length);
    return list[n];
}

function enterThe(matrix) { // For a matrix of numbers return the sum of all numbers as well as the diagonal of the matrix
    var sum = 0,
        diag = [];
    for (var i = 0; i < matrix.length; i++) {
        diag.push(matrix[i][i]);
        for (var j = 0; j < matrix[i].length; j++) {
            sum += matrix[i][j];
        }
    }
    return [sum, diag];
}

function ignoranceIsBliss(matrix, sum, high, low) { // For an input of a matrix and three values
    var out = "<table>";
    for (var i = 0; i < matrix.length; i++) { // Write the matrix to an HTML table
        out += "<tr>";
        for (var j = 0; j < matrix[i].length; j++) {
            out += "<td>" + matrix[i][j] + "</td>";
        }
        out += "</tr>";
    } // Add the first value to a single-cell row, and the other two to a two-cell row
    out += "<tr><td colspan=5 style='font-weight:bold'>&Sigma; " + sum + "</td></tr><tr><td colspan=3>&Delta;&Mu; " + high + "</td><td colspan=2>&Delta;&mu; " + low + "</td></tr></table>";
    return out;
}

function followTheWhiteRabbit(diag) { // For an array of numbers, return the largest and smallest values respectively
    /* for (var i = 0; i < diag.length; i++) {
        if (diag[i] > high) high = diag[i];
        if (diag[i] < low) low = diag[i];
    } */
    return [Math.max(...diag), Math.min(...diag)];
}

function lotsOfGuns(x, y, a, b) { // Create a matrix of dimensions x and y for a range of numbers a to b
    var out = [];
    for (var i = 0; i < x; i++) {
        out[i] = [];
        for (var j = 0; j < y; j++) {
            out[i].push(thereIsNoSpoon(a, b));
        }
    }
    return out;
}

function thereIsNoSpoon(from, to) { // Return random number from range
    return Math.floor((Math.random() * to) + from);
}