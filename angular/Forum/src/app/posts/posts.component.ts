import { Component, OnInit } from "@angular/core";
import { Post } from "../models/data.model";
import { FetchData } from "../data-fetch.service";

@Component({
  templateUrl: "./posts.component.html"
})
export class PostsComponent implements OnInit {
  public errorMessage: any;
  public pageTitle: string = "posts";
  private _postList: Post[];
  constructor(private fetchData: FetchData) {}

  ngOnInit(): void {
    this.fetchData
      .getPosts()
      .subscribe(
        users => (this._postList = users),
        error => (this.errorMessage = <any>error)
      );
  }

  getPosts(): Post[] {
    return this._postList;
  }
}
