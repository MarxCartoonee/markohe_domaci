var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var GeomOblik = /** @class */ (function () {
    function GeomOblik() {
    }
    GeomOblik.prototype.setIme = function (ime) {
        return (this._ime = ime);
    };
    GeomOblik.prototype.getIme = function () {
        return this._ime;
    };
    return GeomOblik;
}());
var Kvadrat = /** @class */ (function (_super) {
    __extends(Kvadrat, _super);
    function Kvadrat(stranica) {
        var _this = _super.call(this) || this;
        _this.setIme("Kvadrat");
        _this._stranica = stranica;
        return _this;
    }
    Kvadrat.prototype.setStranica = function (stranica) {
        return (this._stranica = stranica);
    };
    Kvadrat.prototype.getStranica = function () {
        return this._stranica;
    };
    Kvadrat.prototype.izracunajPovrsinu = function () {
        return Math.pow(this.getStranica(), 2);
    };
    Kvadrat.prototype.prikaziPodatke = function () {
        return this.getIme() + ", stranice " + this.getStranica() + ", ima povrsinu " + this.izracunajPovrsinu().toFixed(2);
    };
    return Kvadrat;
}(GeomOblik));
var Pravougaonik = /** @class */ (function (_super) {
    __extends(Pravougaonik, _super);
    function Pravougaonik(sirina, visina) {
        var _this = _super.call(this) || this;
        _this.setIme("Pravougaonik");
        _this._sirina = sirina;
        _this._visina = visina;
        return _this;
    }
    Pravougaonik.prototype.setSirina = function (sirina) {
        return (this._sirina = sirina);
    };
    Pravougaonik.prototype.setVisina = function (visina) {
        return (this._visina = visina);
    };
    Pravougaonik.prototype.getSirina = function () {
        return this._sirina;
    };
    Pravougaonik.prototype.getVisina = function () {
        return this._visina;
    };
    Pravougaonik.prototype.izracunajPovrsinu = function () {
        return this.getSirina() * this.getVisina();
    };
    Pravougaonik.prototype.prikaziPodatke = function () {
        return this.getIme() + ", stranica " + this.getSirina() + " i " + this.getVisina() + ", ima povrsinu " + this.izracunajPovrsinu().toFixed(2);
    };
    return Pravougaonik;
}(GeomOblik));
var Krug = /** @class */ (function (_super) {
    __extends(Krug, _super);
    function Krug(poluprecnik) {
        var _this = _super.call(this) || this;
        _this.setIme("Krug");
        _this._poluprecnik = poluprecnik;
        return _this;
    }
    Krug.prototype.setPoluprecnik = function (poluprecnik) {
        return (this._poluprecnik = poluprecnik);
    };
    Krug.prototype.getPoluprecnik = function () {
        return this._poluprecnik;
    };
    Krug.prototype.izracunajPovrsinu = function () {
        return Math.pow(this.getPoluprecnik(), 2) * Math.PI;
    };
    Krug.prototype.prikaziPodatke = function () {
        return this.getIme() + ", poluprecnika " + this.getPoluprecnik() + ", ima povrsinu " + this.izracunajPovrsinu().toFixed(2);
    };
    return Krug;
}(GeomOblik));
var Kocka = /** @class */ (function (_super) {
    __extends(Kocka, _super);
    function Kocka(stranica) {
        var _this = _super.call(this, stranica) || this;
        _this.setIme("Kocka");
        return _this;
    }
    Kocka.prototype.izracunajPovrsinu = function () {
        return 6 * Math.pow(this.getStranica(), 2);
    };
    Kocka.prototype.izracunajZapreminu = function () {
        return Math.pow(this.getStranica(), 3);
    };
    Kocka.prototype.prikaziPodatke = function () {
        return this.getIme() + ", stranice " + this.getStranica() + ", ima povrsinu " + this.izracunajPovrsinu().toFixed(2) + " i zapreminu " + this.izracunajZapreminu().toFixed(2);
    };
    return Kocka;
}(Kvadrat));
var Kvadar = /** @class */ (function (_super) {
    __extends(Kvadar, _super);
    function Kvadar(sirina, visina, duzina) {
        var _this = _super.call(this, sirina, visina) || this;
        _this._duzina = duzina;
        _this.setIme("Kvadar");
        return _this;
    }
    Kvadar.prototype.setDuzina = function (duzina) {
        return (this._duzina = duzina);
    };
    Kvadar.prototype.getDuzina = function () {
        return this._duzina;
    };
    Kvadar.prototype.izracunajPovrsinu = function () {
        return (2 * (this.getSirina() * this.getVisina()) +
            2 * (this.getSirina() * this.getDuzina()) +
            2 * (this.getSirina() * this.getDuzina()));
    };
    Kvadar.prototype.izracunajZapreminu = function () {
        return this.getSirina() * this.getVisina() * this.getDuzina();
    };
    Kvadar.prototype.prikaziPodatke = function () {
        return this.getIme() + ", stranica " + this.getSirina() + ", " + this.getVisina() + " i " + this.getDuzina() + ", ima povrsinu " + this.izracunajPovrsinu().toFixed(2) + " i zapreminu " + this.izracunajZapreminu().toFixed(2);
    };
    return Kvadar;
}(Pravougaonik));
var Lopta = /** @class */ (function (_super) {
    __extends(Lopta, _super);
    function Lopta(poluprecnik) {
        var _this = _super.call(this, poluprecnik) || this;
        _this.setIme("Lopta");
        return _this;
    }
    Lopta.prototype.izracunajPovrsinu = function () {
        return 4 * Math.PI * Math.pow(this.getPoluprecnik(), 2);
    };
    Lopta.prototype.izracunajZapreminu = function () {
        return 4 / 3 * Math.pow(this.getPoluprecnik(), 3) * Math.PI;
    };
    Lopta.prototype.prikaziPodatke = function () {
        return this.getIme() + ", poluprecnika " + this.getPoluprecnik() + ", ima povrsinu " + this.izracunajPovrsinu().toFixed(2) + " i zapreminu " + this.izracunajZapreminu().toFixed(2);
    };
    return Lopta;
}(Krug));
function testOutput() {
    var oblici = [
        new Kvadrat(3),
        new Pravougaonik(5, 9),
        new Krug(7),
        new Kocka(11),
        new Kvadar(4, 12, 7),
        new Lopta(12)
    ], output = [];
    for (var _i = 0, oblici_1 = oblici; _i < oblici_1.length; _i++) {
        var oblik = oblici_1[_i];
        output.push(oblik.prikaziPodatke());
    }
    return output.join("\n");
}
console.log(testOutput());
