import { Component, OnInit } from "@angular/core";
import { User } from "../models/data.model";
import { ActivatedRoute, Router } from "@angular/router";
import { FetchData } from "../data-fetch.service";

@Component({
  templateUrl: "./user.component.html"
})
export class UserComponent implements OnInit {
  public errorMessage: any;
  public pageTitle: string = "User not found";
  public user: User;

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _fetchData: FetchData
  ) {}

  ngOnInit(): void {
    let id = Number(this._route.snapshot.paramMap.get("id"));
    if (id) {
      this._fetchData.getUser(id).subscribe(user => {
        this.user = user;
        this.pageTitle = user.username;
      }, error => (this.errorMessage = <any>error));
    }
  }

  back(): void {
    this._router.navigate(["/users"]);
  }
}
