export class PurchaseForm {
  firstName: string;
  lastName: string;
  email: string;
  address: string;
  city: string;
  state?: string;
  zipCode: string;
  country: string;
  creditCard: string;
  expMonth: string;
  expYear: string;
  securityCode: string;
  cardType: string;
  referral?: string;

  constructor() {}
}
