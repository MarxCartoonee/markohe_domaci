import { Component, OnInit } from "@angular/core";
import { FetchData } from "../data-fetch.service";
import { User } from "../models/data.model";

@Component({
  templateUrl: "./users.component.html"
})
export class UsersComponent implements OnInit {
  public errorMessage: any;
  public pageTitle: string = "users";
  private _userList: User[] = [];
  private _filter: string;
  public filterList: User[];
  constructor(private _fetchData: FetchData) {}

  get filter(): string {
    return this._filter;
  }

  set filter(filter: string) {
    this._filter = filter;
  }

  ngOnInit(): void {
    this._fetchData.getUsers().subscribe(users => {
      this._userList = users;
      this.filterList = this._userList;
    }, error => (this.errorMessage = <any>error));
  }

  getUsers(): User[] {
    return this._userList;
  }

  search(term: string): void {
    this.filterList = this.filterUsers(term);
  }

  filterUsers(term: string): User[] {
    term = term.toLocaleLowerCase();
    return this._userList.filter(
      (user: User) => user.username.toLocaleLowerCase().indexOf(term) !== -1
    );
  }
}
