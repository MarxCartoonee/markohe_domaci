import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "hrefify"
})
export class Hrefify implements PipeTransform {
  transform(input: string, option: string = "") {
    if (option) option += ":";
    if (option == "tel:") input = input.split(" ")[0];
    if (option == "http:") option += "//";
    return `<a href="${option + input}">${input}</a>`;
  }
}
