import { Component } from "@angular/core";
import { Image } from "../models/data.model";
import { FetchData } from "../data-fetch.service";

@Component({
  templateUrl: "./photos.component.html"
})
export class PhotosComponent {
  public errorMessage: any;
  public pageTitle: string = "photos";
  private _photoList: Image[];
  constructor(private _fetchData: FetchData) {}

  ngOnInit(): void {
    this._fetchData
      .getImages()
      .subscribe(
        photos => (this._photoList = photos),
        error => (this.errorMessage = <any>error)
      );
  }

  getPhotos(): Image[] {
    return this._photoList;
  }
}
