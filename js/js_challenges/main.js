function Fill(myself, func) {
    var myValue = document.getElementById(myself).value,
        myArg;
    if ((myArg = document.getElementById(myself + "-in"))) myArg = myArg.value;
    document.getElementById(myself + "-out").value = func(myValue, myArg);
}

function FirstReverse(str) {
    str = str.split("").reverse().join("");
    return str;
}

function LetterChanges(str, num) {
    num = parseInt(num);
    let newStr = [],
        chrChk,
        chrChng,
        chrNu;

    for (let i = 0; i < str.length; i++) {
        chrChk = str.charAt(i);
        chrChng = str.charCodeAt(i);
        chrNu = chrChng + num;

        if (chrChk.match("\[A-Za-z\]")) {
            if ((chrChng < 90 && chrNu > 90) || (chrChng < 122 && chrNu > 122)) chrNu -= 26;
            // chrChk = String.fromCharCode(chrNu);
        }
        newStr.push(chrChk);
    }
    str = String.fromCharCode(newStr);
    return str;
}

function LetterCapitalize(str) {
    if (str.trim().length > 0) {
        str = str.split(" ");
        for (var i = 0; i < str.length; i++) {
            str[i] = str[i].split("");
            str[i][0] = str[i][0].toUpperCase();
            str[i] = str[i].join("");
        }
        str = str.join(" ");
    }
    return str;
}

function LongestWord(sen) {
    sen = sen.split(" ");
    sen.forEach(element, i => {

    });
}

// ROT STRING
$(function() {
    $('#rot .rot-in').on('input', function() {
        $('#rot #rot-out').val(function() {
            // let newStr = [],
            //     chrChk,
            //     chrChng,
            //     chrNu;

            // let str = $('#rot #rot-in').val()
            // num = $('#rot #rot-num').val();

            // for (let i = 0; i < str.length; i++) {
            //     newStr.push(str.charCodeAt(i) + num);

            //     if (chrChk.match("\[A-Za-z\]")) {
            //         if ((chrChng < 90 && chrNu > 90) || (chrChng < 122 && chrNu > 122)) chrNu -= 26;
            //         // chrChk = String.fromCharCode(chrNu);
            //     }
            // }
            // str = String.fromCharCode(newStr);
            // return str;
        });
    });

    $('#rot #rot-num').slider({
        value: 0,
        min: 0,
        max: 26,
        step: 1,
        slide: function(event, ui) {
            $('#rot #rot-num-out').val(ui.value);
        }
    });
});