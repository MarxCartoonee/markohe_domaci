document.write(phi2(100));

function phi2(max) {
    var a = 0,
        b = 1,
        out = [];
    do {
        out.push(a, b);
        [a, b] = [a += b, b += a];
    } while (a < max && b < max);
    return out.join(", ");
}