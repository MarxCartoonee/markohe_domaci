function numBar(numList) {
    for (var i = 0; i < numList.length; i++) {
        document.write(numCheck(numList[i]));
        if ((i + 1) % 100 == 0) document.body.appendChild(document.createElement("HR"));
    }
}

function numCheck(num) {
    var out = "";
    if (num % 4 == 0 && num % 6 != 0) {
        out = num;
        if (num % 1000 != 0) out += ", ";
    }
    return out;
}

function phi(max) {
    var a = 0,
        b = 1,
        newSpanA,
        newSpanB;
    do {
        newSpanA = document.createElement("SPAN");
        newSpanB = document.createElement("SPAN");
        newSpanA.appendChild(document.createTextNode(a + " "));
        newSpanB.appendChild(document.createTextNode(b + " "));
        newSpanA.style.fontSize = (1 + a / 10) + "em";
        newSpanB.style.fontSize = (1 + b / 10) + "em";
        document.body.appendChild(newSpanA);
        if (b < 100) document.body.appendChild(newSpanB);

        a += b;
        b += a;
    } while (a < max);
}

function range(low, high) {
    var out = [];

    do {
        out.push(low++);
    } while (low <= high);

    return out;
}

numBar(range(200, 1000));
document.body.appendChild(document.createElement("HR"));
phi(100);