import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "capitalize"
})
export class Capitalize implements PipeTransform {
  transform(input: string, which?: string) {
    if (which == "all") return input.replace(/\b\w/g, l => l.toUpperCase());
    else return input.charAt(0).toUpperCase() + input.substr(1);
  }
}
