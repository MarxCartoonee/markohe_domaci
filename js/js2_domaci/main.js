var namesList = ["Mića Jolić", "Dragana Ristić", "Nebojša Marinkov"],
    numList = [17, 385],
    triangle = [11, 39],
    visibleOutput = "";

// BEGIN
for (i = 0; i < namesList.length; i++) {
    nameOut(namesList[i]);
}

numSum(numList[0], numList[1]);
pyGo(triangle[0], triangle[1]);

document.getElementById("zadatak20").innerHTML = visibleOutput;

// FUNCTIONS
function nameOut(nameIn) {
    printSum(nameIn);
}

function numSum(numA, numB) {
    printSum("Zbir brojeva " + numA + " i " + numB + " je " + (numA + numB));
}

function pyGo(sideA, sideB) {
    printSum("Dužina hipotenuze trougla sa stranicama dužine" + sideA + " i " + sideB + " je " + Math.sqrt((sideA ** 2 + sideB ** 2)));
}

function printSum(toPrint) {
    visibleOutput += toPrint + "\n"; // TODO Trim trailing newline
    console.info(toPrint);
}