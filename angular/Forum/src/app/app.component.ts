import { Component } from "@angular/core";
import { FetchData } from "./data-fetch.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"],
  providers: [FetchData]
})
export class AppComponent {
  title = "app";
}
