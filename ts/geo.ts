class GeomOblik {
  private _ime: string;

  setIme(ime: string): string {
    return (this._ime = ime);
  }

  getIme(): string {
    return this._ime;
  }
}

class Kvadrat extends GeomOblik implements ImaPovrsinu {
  private _stranica: number;

  constructor(stranica: number) {
    super();
    this.setIme("Kvadrat");
    this._stranica = stranica;
  }

  setStranica(stranica: number): number {
    return (this._stranica = stranica);
  }

  getStranica(): number {
    return this._stranica;
  }

  izracunajPovrsinu(): number {
    return this.getStranica() ** 2;
  }

  prikaziPodatke(): string {
    return `${this.getIme()}, stranice ${this.getStranica()}, ima povrsinu ${this.izracunajPovrsinu().toFixed(
      2
    )}`;
  }
}

class Pravougaonik extends GeomOblik implements ImaPovrsinu {
  private _sirina: number;
  private _visina: number;

  constructor(sirina: number, visina: number) {
    super();
    this.setIme("Pravougaonik");
    this._sirina = sirina;
    this._visina = visina;
  }

  setSirina(sirina: number): number {
    return (this._sirina = sirina);
  }

  setVisina(visina: number): number {
    return (this._visina = visina);
  }

  getSirina(): number {
    return this._sirina;
  }

  getVisina(): number {
    return this._visina;
  }

  izracunajPovrsinu(): number {
    return this.getSirina() * this.getVisina();
  }

  prikaziPodatke(): string {
    return `${this.getIme()}, stranica ${this.getSirina()} i ${this.getVisina()}, ima povrsinu ${this.izracunajPovrsinu().toFixed(
      2
    )}`;
  }
}

class Krug extends GeomOblik implements ImaPovrsinu {
  private _poluprecnik: number;

  constructor(poluprecnik: number) {
    super();
    this.setIme("Krug");
    this._poluprecnik = poluprecnik;
  }

  setPoluprecnik(poluprecnik: number): number {
    return (this._poluprecnik = poluprecnik);
  }

  getPoluprecnik(): number {
    return this._poluprecnik;
  }

  izracunajPovrsinu(): number {
    return this.getPoluprecnik() ** 2 * Math.PI;
  }

  prikaziPodatke(): string {
    return `${this.getIme()}, poluprecnika ${this.getPoluprecnik()}, ima povrsinu ${this.izracunajPovrsinu().toFixed(
      2
    )}`;
  }
}

class Kocka extends Kvadrat implements ImaZapreminu {
  constructor(stranica: number) {
    super(stranica);
    this.setIme("Kocka");
  }

  izracunajPovrsinu(): number {
    return 6 * this.getStranica() ** 2;
  }

  izracunajZapreminu(): number {
    return this.getStranica() ** 3;
  }

  prikaziPodatke(): string {
    return `${this.getIme()}, stranice ${this.getStranica()}, ima povrsinu ${this.izracunajPovrsinu().toFixed(
      2
    )} i zapreminu ${this.izracunajZapreminu().toFixed(2)}`;
  }
}

class Kvadar extends Pravougaonik implements ImaZapreminu {
  private _duzina: number;

  constructor(sirina: number, visina: number, duzina: number) {
    super(sirina, visina);
    this._duzina = duzina;
    this.setIme("Kvadar");
  }

  setDuzina(duzina: number): number {
    return (this._duzina = duzina);
  }

  getDuzina(): number {
    return this._duzina;
  }

  izracunajPovrsinu(): number {
    return (
      2 * (this.getSirina() * this.getVisina()) +
      2 * (this.getSirina() * this.getDuzina()) +
      2 * (this.getSirina() * this.getDuzina())
    );
  }

  izracunajZapreminu(): number {
    return this.getSirina() * this.getVisina() * this.getDuzina();
  }

  prikaziPodatke(): string {
    return `${this.getIme()}, stranica ${this.getSirina()}, ${this.getVisina()} i ${this.getDuzina()}, ima povrsinu ${this.izracunajPovrsinu().toFixed(
      2
    )} i zapreminu ${this.izracunajZapreminu().toFixed(2)}`;
  }
}

class Lopta extends Krug implements ImaZapreminu {
  constructor(poluprecnik: number) {
    super(poluprecnik);
    this.setIme("Lopta");
  }

  izracunajPovrsinu(): number {
    return 4 * Math.PI * this.getPoluprecnik() ** 2;
  }

  izracunajZapreminu(): number {
    return 4 / 3 * this.getPoluprecnik() ** 3 * Math.PI;
  }

  prikaziPodatke(): string {
    return `${this.getIme()}, poluprecnika ${this.getPoluprecnik()}, ima povrsinu ${this.izracunajPovrsinu().toFixed(
      2
    )} i zapreminu ${this.izracunajZapreminu().toFixed(2)}`;
  }
}

interface ImaPovrsinu {
  izracunajPovrsinu(): number;
  prikaziPodatke(): string;
}

interface ImaZapreminu {
  izracunajZapreminu(): number;
  prikaziPodatke(): string;
}

function testOutput(): string {
  let oblici = [
      new Kvadrat(3),
      new Pravougaonik(5, 9),
      new Krug(7),
      new Kocka(11),
      new Kvadar(4, 12, 7),
      new Lopta(12)
    ],
    output = [];

  for (let oblik of oblici) {
    output.push(oblik.prikaziPodatke());
  }

  return output.join("\n");
}

console.log(testOutput());
